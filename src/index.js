import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './AppContainer';
import * as serviceWorker from './serviceWorker';



import { Provider } from 'react-redux'
import store from './store/store';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import themes, { overrides } from './themes';
// import setAuthToken from './helpers/setAuthToken';
// import jwt_decode from 'jwt-decode';

const theme = createMuiTheme({...themes.default, ...overrides});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
	  <Provider store={store}>
  	  		<App />
 		 </Provider>
    </MuiThemeProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
