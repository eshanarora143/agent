import React, { Component } from "react";
import { withStyles } from "@material-ui/core";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// Pages
import OverView from "../../pages/overview/overView";

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const classes = this.props.classes;
    return (
      <div className={classes.root}>
        <BrowserRouter>
          <React.Fragment>
          
              <Switch>
                <Route path="/bot/overview" component={OverView} />
              </Switch>
          </React.Fragment>
        </BrowserRouter>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    display: "flex",
    width: "100vw",
    height: "100vh",
    overflowX: "hidden"
  },
  content: {
    flexGrow: 1,
    width: `calc(100vw - 240px)`
    // minHeight: '100vh',
  },
  contentShift: {
    width: `calc(100vw - ${240 + theme.spacing(10)}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  }
});

export default withStyles(styles)(Layout);
