import { connect } from 'react-redux';

import { toggleSidebar } from './layoutState';

import LayoutView from './layoutView';


export default connect(
    state => ({
      isSidebarOpened: state.layout.isSidebarOpened,
    }),
    { toggleSidebar },
  )(LayoutView);