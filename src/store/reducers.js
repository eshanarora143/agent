import { combineReducers } from 'redux';

import layout from '../components/layout/layoutState';
import login from '../pages/login/loginState';
import fetch from './fetchState'
export default combineReducers({
  layout,
  login,
  fetch,
});
