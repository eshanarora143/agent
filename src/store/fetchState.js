import axios from "axios";
export const initialState = {
  isLoading: true,
  data: [],
  filteredData: {},
  error: false,
  max: 0,
  avg: 0,
  chartData: []
};

export const START_FETCH = "Fetch/START_FETCH";
export const FETCH_SUCCESS = "Fetch/FETCH_SUCCESS";
export const FETCH_ERROR = "Fetch/FETCH_ERROR";
export const SET_STAT = "Fetch/SET_STAT";

export const startFetch = () => ({
  type: START_FETCH
});

export const fetchSuccess = payload => ({
  type: FETCH_SUCCESS,
  payload
});

export const fetchError = () => ({
  type: FETCH_ERROR
});
export const setStat = payload => ({
  type: SET_STAT,
  payload
});

export const fetchData = (payload, url) => dispatch => {
  console.log("fetch data called");
  console.log(payload);
  dispatch(startFetch());
  const baseURL = "https://react-integration.herokuapp.com";
  // const baseURL = "http://localhost:5000";
  axios
    .post(baseURL + url, payload)
    .then(res => {
      // console.log(res);
      dispatch(fetchSuccess(res.data.data));
      return res;
    })
    .catch(err => {
      dispatch(fetchError());
    });
};
// // Set statistics value in case of leads and sessions
export const setStatData = payload => dispatch => {
  console.log("set state data", payload);
};

export default function FetchReducer(state = initialState, { type, payload }) {
  switch (type) {
    case START_FETCH:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: payload,
        error: null
      };
    case FETCH_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    case SET_STAT:
      return {
        ...state,
        max: payload.max,
        avg: payload.avg,
        chartData: payload.chartData,
        filteredData: payload.chartData
      };
    default:
      return state;
  }
}
