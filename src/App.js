import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import {withRouter} from 'react-router' // further imports omitted

import Layout from './components/layout/layoutView';
// import Error from '../pages/error';
import Login from './pages/login/loginContainer';



const PrivateRoute = ({ component, isAuthenticated, ...rest }) => {
  // console.log(isAuthenticated)
  return (
    <Route
      {...rest} render={props => (
      isAuthenticated ? (
        React.createElement(component, props)
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location },
          }}
        />
      )
    )}
    />
  );
};

const PublicRoute = ({ component, isAuthenticated, ...rest }) => {
  return (
    <Route
      {...rest} render={props => (
      isAuthenticated ? (
        <Redirect
          to={{
            pathname: '/',
          }}
        />
      ) : (
        React.createElement(component, props)
      )
    )}
    />
  );
};

const bot = (auth , ...props) => {
  return(
      <BrowserRouter>
        <Switch>
         
          <Route exact path="/" render={() => <Redirect to="/bot/overview" />} />
          <Route exact path="/bot" render={() => <Redirect to="/bot/overview" />} />
          <PrivateRoute path="/bot" component={Layout} isAuthenticated={false}/>
          <PublicRoute path="/login" component={Login} isAuthenticated={false}/>
        </Switch>
      </BrowserRouter>
  )

}

export default bot;
