import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import compose from 'recompose/compose';

import LoginView from "./loginView";
import { loginUser, resetError } from "./loginState";

export default compose(
  connect(
    state => ({
      isLoading: state.login.isLoading,
      isAuthenticated: state.login.isAuthenticated,
      error: state.login.error
    }),
    { loginUser, resetError }
  ),
  withRouter,
)(LoginView);