import React, { Component } from "react";
import {
  Grid,
  CircularProgress,
  Typography,
  withStyles,
  Button,
  Tabs,
  Tab,
  TextField,
  Fade,
  Avatar
} from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'

// import google from "../../images/google.svg";

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
		  activeTabId: 0,
		  nameValue: "",
		  loginValue: "",
		  passwordValue: ""
		}
	}
    handleInput = (e, input = "login") => {
      if (this.props.error) {
        this.props.resetError();
      }

      if (input === "login") {
        this.setState({loginValue : e.target.value })
      } else if (input === "password") {
        this.setState({passwordValue : e.target.value })
      } else if (input === "name") {
        this.setState({nameValue : e.target.value })
      }
    }
    handleLoginButtonClick = ()=>{
      this.props.loginUser(this.state.loginValue, this.state.passwordValue);
    }
	render(){
		const classes = this.props.classes
		return(
			<Grid container className={classes.container}>
				<div className={classes.logotypeContainer}>
				</div>
				<div className={classes.formContainer}>
			        <Avatar className={classes.avatar}>
			          <LockOutlinedIcon />
			        </Avatar>
			        <Typography component="h1" variant="h5">
			          	Sign in
					</Typography>
				  <div className={classes.form}>
				        <Fade in={this.props.error}>
				          <Typography color="secondary" className={classes.errorMessage}>
				            Something is wrong with your login or password :(
				          </Typography>
				        </Fade>
				        <TextField
				          id="email"
				          variant= "outlined"
				          label= "Email"
				          value={this.state.loginValue}
				          onChange={e => this.handleInput(e, "login")}
				          margin="normal"
				          placeholder="Email Adress"
				          type="email"
				          fullWidth
				        />
				        <TextField
				          id="password"
				          variant= "outlined"
				          label= "Password"
				          value={this.state.passwordValue}
				          onChange={e => this.handleInput(e, "password")}
				          margin="normal"
				          placeholder="Password"
				          type="password"
				          fullWidth
				        />
				        <div className={classes.formButtons}>
				          {this.props.isLoading ? (
				            <CircularProgress size={26} className={classes.loginLoader} />
				          ) : (
				            <Button
				              disabled={
				                this.state.loginValue.length === 0 ||
				                this.state.passwordValue.length === 0
				              }
				              onClick={this.handleLoginButtonClick}
				              variant="contained"
				              color="primary"
				              size="large"
				            >
				              Login
				            </Button>
				          )}
				        </div>
				  </div>
				  <Typography  className={classes.copyright}>
				    © 2019-2020 Triny. All rights reserved.
				  </Typography>
				</div>
			</Grid>	
		)
	
	}

}

const styles = theme => ({
  container: {
    height: "100vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: 0,
    left: 0
  },
  logotypeContainer: {
    backgroundColor: theme.palette.primary.main,
    // backgroundImage: `url(${Banner})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    width: "60%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "50%"
    },
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  logotypeImage: {
    width: 165,
    marginBottom: theme.spacing(4)
  },
  logotypeText: {
    color: "white",
    fontWeight: 500,
    fontSize: 84,
    [theme.breakpoints.down("md")]: {
      fontSize: 48
    }
  },
  formContainer: {
    width: "40%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "50%"
    }
  },
  form: {
    width: 320
  },
  tab: {
    fontWeight: 400,
    fontSize: 18
  },
  avatar: {
	margin: theme.spacing(1),
	textAlign: "center",
	backgroundColor: theme.palette.secondary.main,  	
  },
  greeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(4),
  },
  subGreeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(2)
  },
  googleButton: {
    marginTop: theme.spacing(6),
    // boxShadow: theme.customShadows.widget,
    backgroundColor: "white",
    width: "100%",
    textTransform: "none"
  },
  googleButtonCreating: {
    marginTop: 0
  },
  googleIcon: {
    width: 30,
    marginRight: theme.spacing(2)
  },
  creatingButtonContainer: {
    marginTop: theme.spacing(2.5),
    height: 46,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  createAccountButton: {
    height: 46,
    textTransform: "none"
  },
  formDividerContainer: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    display: "flex",
    alignItems: "center"
  },
  formDividerWord: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  formDivider: {
    flexGrow: 1,
    height: 1,
    backgroundColor: theme.palette.text.hint + "40"
  },
  errorMessage: {
    textAlign: "center"
  },
  formButtons: {
    width: "100%",
    marginTop: theme.spacing(4),
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  forgetButton: {
    textTransform: "none",
    fontWeight: 400
  },
  loginLoader: {
    marginLeft: theme.spacing(4)
  },
  copyright: {
    marginTop: theme.spacing(4),
    whiteSpace: 'nowrap',
    fontSize: 13,
    [theme.breakpoints.up("md")]: {
      position: "absolute",
      bottom: theme.spacing(2),
    }
  }
});

export default withStyles(styles, { withTheme: true })(Login);
