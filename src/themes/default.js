import tinycolor from "tinycolor2";

// const primary = "#536DFE";
const pending = "#d32f2f";
const resolved = "#388e3c";
const primary = "#039be5";
const secondary = "#FF5C93";
const warning = "#FFC260";
const success = "#3CD4A0";
const info = "#9013FE";
const backdrop = "#f6f7ff";
const lightenRate = 7.5;
const darkenRate = 15;
const queryText = "#e0e0e0";

export default {
  palette: {
    pending: {
      main: pending,
      light: tinycolor(pending)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(pending)
        .darken(darkenRate)
        .toHexString()
    },
    resolved: {
      main: resolved,
      light: tinycolor(resolved)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(resolved)
        .darken(darkenRate)
        .toHexString()
    },

    primary: {
      main: primary,
      light: tinycolor(primary)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(primary)
        .darken(darkenRate)
        .toHexString()
    },
    secondary: {
      main: secondary,
      light: tinycolor(secondary)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(secondary)
        .darken(darkenRate)
        .toHexString(),
      contrastText: "#FFFFFF"
    },
    warning: {
      main: warning,
      light: tinycolor(warning)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(warning)
        .darken(darkenRate)
        .toHexString()
    },
    success: {
      main: success,
      light: tinycolor(success)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(success)
        .darken(darkenRate)
        .toHexString()
    },
    info: {
      main: info,
      light: tinycolor(info)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(info)
        .darken(darkenRate)
        .toHexString()
    },
    text: {
      primary: "#4A4A4A",
      secondary: "#6E6E6E",
      hint: "#B9B9B9"
    },
    background: {
      default: "#F6F7FF",
      light: "#F3F5FF"
    },
    backdrop: {
      default: "#f6f7ff"
    }
  },
  customShadows: {
    widget:
      "0px 3px 11px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A",
    widgetDark:
      "0px 3px 18px 0px #4558A3B3, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A",
    widgetWide:
      "0px 12px 33px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A"
  },
  overrides: {
    MuiBackdrop: {
      root: {
        backgroundColor: "#4A4A4A1A"
      }
    },
    // MuiMenu: {
    //   paper: {
    //     boxShadow: "none"
    //     // "0px 3px 11px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A"
    //   }
    // },
    MuiMenu: {
      list: {
        padding: "0px"
      }
    },
    MuiSelect: {
      icon: {
        color: "#B9B9B9"
      }
    },

    MuiListItem: {
      button: {
        "&:hover, &:focus": {
          backgroundColor: "#EAEEF2"
          // background: "linear-gradient(45deg, #eeeeee 30%, #e0e0e0 90%)"
        }
      },
      selected: {
        backgroundColor: "#1565c0 !important",
        "&:focus": {
          backgroundColor: "#1565c0"
        }
      }
    },
    MuiTouchRipple: {
      child: {
        backgroundColor: "white"
      }
    },
    MuiTableRow: {
      root: {
        height: 56
      }
    },
    MuiTableCell: {
      root: {
        borderBottom: "1px solid rgba(224, 224, 224, .5)"
      },
      head: {
        fontSize: "0.95rem"
      },
      body: {
        fontSize: "0.95rem"
      }
    }
  }
};
