import { connect } from 'react-redux';

import AppView from './App';

export default connect(
    state => ({
      isAuthenticated: state.login.isAuthenticated,
    })
  )(AppView)